# Formatted RSS to Telegram

## ENV
```shell
DATABASE=
TG_TOKEN=
TG_CHAT_ID=
TG_DISABLE_NOTIFICATION=false
TG_DISABLE_WEB_PAGE_PREVIEW=false
TG_PARSE_MODE=MarkdownV2

USER-AGENT="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36"

EXPIRE_TIME=30d
```