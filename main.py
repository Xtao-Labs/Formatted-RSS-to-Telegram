from FR2T import fr2t
from time import sleep

if __name__ == "__main__":
    config_path = "data/config.yaml"
    rss_path = "data/rss.yaml"

    fr = fr2t.FR2T(config_path, rss_path)
    while True:
        fr.run()
        sleep(600)
